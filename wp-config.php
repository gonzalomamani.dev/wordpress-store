<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'store' );

/** MySQL database username */
define( 'DB_USER', 'remoto' );

/** MySQL database password */
define( 'DB_PASSWORD', '123' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PU#vcGo7pX3=58-&[Z:]M:Sw(5^T5^i!:{rpPI)!dv~nVYo8~O@SSM4r-&Y=Ah$X');
define('SECURE_AUTH_KEY',  'JAOs<jy0sj<}}-4ZGl:Mz/U&I_s~=UTAEv%0[w8[Nuy^hk(LzONL!9Y%[-j-?n9i');
define('LOGGED_IN_KEY',    'x_T Gxvc%-qyFmI`-92=#PHbv:.S8S5.^]+=^FmLvu|fD-E?GV.nQW ]=2U-,oW^');
define('NONCE_KEY',        '7).RmYu4L7-7As(AQC2X>lf,-$t+-970n{=>t8yvhjS|.%)|9Ze#JE3D|{W>;39B');
define('AUTH_SALT',        'w!B kuzCeHT$JE.h80@569x)v|1T+ey=B?73H/3Rb ?B2w,PTzg+ps6$aGrFQnTV');
define('SECURE_AUTH_SALT', 'g2Eb6AE==f(J9r-QTLa+v25c6|P;M>3G!||/DJqAD{r5zd}+)oG)Tx,-EN?h,E7E');
define('LOGGED_IN_SALT',   '`NM|F|mz [MmN#X5mN|Od<@G}E;e-t+7}tFEUmW8w$&ME5Sbm+hsh|BDj)ly;!Pw');
define('NONCE_SALT',       'M>x(DWMT:u0G{: <H|3^u)8_bN+CkB|&[y0c>{n>`R(YC Z$MsY=2)+IVK,.OHtW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
